fetch('https://jsonplaceholder.typicode.com/todos')
.then(res => res.json())

.then(data => {
     console.log(data)
 })

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(res => res.json())

.then(data => {
     console.log(data)
 })

fetch('https://jsonplaceholder.typicode.com/todos', {
     method: 'POST',
     headers: {
          'Content-type': 'application/json'
     },
     body: JSON.stringify({
          title:'Activity s28',
          body: 'This is a hard activity ',
          userId: 1
     })
})
.then(res => res.json())
.then(data => console.log(data))

fetch('https://jsonplaceholder.typicode.com/todos/1', {
     method: 'PUT',
     headers: {
          'Content-type': 'application/json'
     },
     body: JSON.stringify({
          title:'Proof of Update',
          description: 'Hi, I am updated',
          dateCompleted: '3/12/2021',
          userId:1
     })

})
.then(res => res.json())
.then(data => console.log(data))

fetch('https://jsonplaceholder.typicode.com/todos/1', {
     method: 'PATCH',
     headers: {
          'Content-type': 'application/json'
     },
     body: JSON.stringify({
          title:'Proof of Update again',
     })

})
.then(res => res.json())
.then(data => console.log(data))


fetch('https://jsonplaceholder.typicode.com/todos/2', {
     method: 'PUT',
     headers: {
          'Content-type': 'application/json'
     },
     body: JSON.stringify({
          status:'Complete',
          dateChanged:'3/12/21'
     })

})
.then(res => res.json())
.then(data => console.log(data))


fetch('https://jsonplaceholder.typicode.com/posts/3', {method: 'DELETE'})
.then(res=> res.json())
.then(data=>console.log(data))

